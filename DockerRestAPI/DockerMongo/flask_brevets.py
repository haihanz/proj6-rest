"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging
import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    date = arrow.get(begin_date + begin_time)
    brevet_dist_km = request.args.get('brevet_dist_km')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, date)
    close_time = acp_times.close_time(km, brevet_dist_km, date)
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


#############
@app.route('/wrong')
def wrong():
    return render_template('nosubmit.html')
@app.route('/_submit')
def _submit():
    db.tododb.drop()
    dictionary = request.args.get('dictionary')
    # if dictionary == "":
    #     return 
    distance = request.args.get('distance')
    distance_doc = {'distance':distance}
    db.tododb.insert_one(distance_doc)
    result = dictionary.split(',')
    for i in range(0,len(result),4):
        new_dic = {}
        new_dic['mile'] = result[i]
        new_dic['km'] = result[i+1]
        new_dic['open'] = result[i+2]
        new_dic['close'] = result[i+3]
        db.tododb.insert_one(new_dic)
    return flask.jsonify()

@app.route('/_display')
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

################################################

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=80, host="0.0.0.0", debug=True)



