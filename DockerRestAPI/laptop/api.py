# Laptop Service

from flask import Flask, request
from flask_restful import Resource, Api
from flask_restful import reqparse
import csv
from pymongo import MongoClient
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        closetime = []
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
                closetime.append(item['close'])
        return {"open": opentime, "close":closetime}
    
class listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(opentime[i])
            return new

        return {"opentime" : opentime}


class listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        closetime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                closetime.append(item['close'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(closetime[i])
            return new
    
        return {"closetime" : closetime}

        
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

# reference: https://docs.python.org/3/library/csv.html

class csv_listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        alltimes = []
    
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                alltimes.append((item['open'] + ', ' + item['close']))
        with open('all.csv','w', newline = '')as csvfile:
            fieldnames = ['Alltimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in alltimes:
                writer.writerow({'Alltimes': item})

        return ['opentimes' + ',' + 'closetimes'] + alltimes
        

class csv_listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(opentime[i+1])
            with open('open.csv','w', newline = '')as csvfile:
                fieldnames = ['opentimes']
                writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
                writer.writeheader()
                for item in new:
                    writer.writerow({'opentimes': item})    
            return ['opentimes'] + new
  
        with open('open.csv','w', newline = '')as csvfile:
            fieldnames = ['opentimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in opentime:
                writer.writerow({'opentimes': item})

        return ['opentimes'] + opentime

class csv_listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        closetime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                closetime.append(item['close'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(closetime[i+1])
            with open('close.csv','w', newline = '')as csvfile:
                fieldnames = ['closetimes']
                writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
                writer.writeheader()
                for item in new:
                    writer.writerow({'closetimes': item})
            return ['closetimes'] + new

        with open('close.csv','w', newline = '')as csvfile:
            fieldnames = ['closetimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in closetime:
                writer.writerow({'closetimes': item})
    
        return ['closetimes'] + closetime

api.add_resource(csv_listAll, '/listAll/csv')
api.add_resource(csv_listOpenOnly, '/listOpenOnly/csv')
api.add_resource(csv_listCloseOnly, '/listCloseOnly/csv')



# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

