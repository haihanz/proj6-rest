## ACP time caculator
A caculator for riders to calculate their max time and min time in provide distance.

# Author information
Credits to Michal Young for the initial version of this code.
Author: Haihan Zhai
E-mail: haihanz@uoregon.edu

# Rule
control: 200, 400, 600, 1000

0-200 km, the min speed is 15 km/hr, max speed 34 km/hr
200-400 km, the min speed is 15 km/hr, max speed 32 km/hr
400-600 km, the min speed is 15 km/hr, max speed 30 km/hr
600-1000 km, the min speed is 11.428 km/hr, max speed 28 km/hr

eg. if the control at 600km, the distance is 350 km, therefore, the opening 
	time is 200/34 + 150/32 = 10H34. The result time show as ISO 8601 form.

Firt you need to give a control(limit distance), and provide a begin date and begin time.
And then, input the miles or km you want, remember, the input should be progressive increase,
and the last input should less or equal to the control. Finally, you will get the opening and closing
time for your different check point.

## Main Document Introduction

#website folder
acp_times.py: include two functions, one is for caclute the opening time, the other one for closing time.
opening time with the max speed and the clasing time with the min speed.

flask_brevets.py: implement the ajax in our flask_brevets.py, when you submit, you will send the data to the database, when you display, you will see the result.

calc.html: the ACP time caculator form, and implement the javascript in calc.html

# Ports
APP web:
	0.0.0.0:5000
API web:
	0.0.0.0:5001
Consumer Web:
	0.0.0.0:5002


#labtop folder
api.py: we build api in this file, so we can see the data we need in direct ways.

# Nosetests

Run nosetests test_acp_times.py under the brevets folder. 

# References
The more information about rules and background are here(https://rusa.org/pages/acp-brevet-control-times-calculator).
The rules for riders are here(https://rusa.org/pages/rulesForRiders).
You can use the original one to test the output: brevet time calculator(https://rusa.org/octime_acp.html).



